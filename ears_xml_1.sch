EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 11774 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	8400 2800 8300 2800
Wire Wire Line
	3000 2900 3100 2900
Wire Wire Line
	3500 3100 3500 3000
Wire Wire Line
	3500 3000 3800 3000
Wire Wire Line
	5500 1900 5600 1900
Wire Wire Line
	5700 2200 5500 2200
Connection ~ 5500 2200
Wire Wire Line
	3400 2800 3400 3100
Wire Wire Line
	7200 1900 7200 2300
Wire Wire Line
	7200 2300 7200 2600
Wire Wire Line
	7100 2600 7200 2600
Wire Wire Line
	7000 2300 7200 2300
Wire Wire Line
	6700 1900 7200 1900
Connection ~ 7200 2300
Connection ~ 7200 1900
Connection ~ 7200 2600
Text GLabel 8200 1900 2    70   BiDi ~ 0
AMP_OUT
Wire Wire Line
	8300 2600 8400 2600
Wire Wire Line
	4100 2700 4100 2800
Wire Wire Line
	3000 2700 4100 2700
Wire Wire Line
	4300 2700 4100 2700
Connection ~ 4100 2700
Wire Wire Line
	6500 2500 6400 2500
Wire Wire Line
	6400 2500 6400 2300
Wire Wire Line
	6400 2300 6400 2200
Wire Wire Line
	6400 2200 6400 2100
Wire Wire Line
	6100 2200 6400 2200
Wire Wire Line
	6700 2300 6400 2300
Connection ~ 6400 2300
Connection ~ 6400 2200
Wire Wire Line
	6000 1900 6100 1900
Wire Wire Line
	4800 2700 4700 2700
Wire Wire Line
	4700 2800 4700 2700
Wire Wire Line
	4600 2700 4700 2700
Connection ~ 4700 2700
$Comp
L ears_xml-eagle-import:PJ301_THONKICONN6 J2
U 1 1 F2D55355
P 8600 2700
AR Path="/F2D55355" Ref="J2"  Part="1" 
AR Path="/5EF91E25/F2D55355" Ref="J2"  Part="1" 
F 0 "J2" H 8500 2860 59  0000 L BNN
F 1 "PJ301_THONKICONN6" H 8600 2700 50  0001 C CNN
F 2 "ears_xml:MJ-3536N" H 8600 2700 50  0001 C CNN
F 3 "" H 8600 2700 50  0001 C CNN
	1    8600 2700
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:PJ301_THONKICONN6 J1
U 1 1 2CE6A523
P 2800 2800
AR Path="/2CE6A523" Ref="J1"  Part="1" 
AR Path="/5EF91E25/2CE6A523" Ref="J1"  Part="1" 
F 0 "J1" H 2700 2960 59  0000 L BNN
F 1 "PJ301_THONKICONN6" H 2800 2800 50  0001 C CNN
F 2 "ears_xml:MJ-3536N" H 2800 2800 50  0001 C CNN
F 3 "" H 2800 2800 50  0001 C CNN
	1    2800 2800
	-1   0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:R-US_R0402 R1
U 1 1 9D6B0345
P 8100 2600
AR Path="/9D6B0345" Ref="R1"  Part="1" 
AR Path="/5EF91E25/9D6B0345" Ref="R1"  Part="1" 
F 0 "R1" H 7950 2659 59  0000 L BNN
F 1 "510" H 7950 2470 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 8100 2600 50  0001 C CNN
F 3 "" H 8100 2600 50  0001 C CNN
	1    8100 2600
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:R-US_R0402 R5
U 1 1 1855BC5E
P 4700 3000
AR Path="/1855BC5E" Ref="R5"  Part="1" 
AR Path="/5EF91E25/1855BC5E" Ref="R5"  Part="1" 
F 0 "R5" H 4550 3059 59  0000 L BNN
F 1 "1.0M" H 4550 2870 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 4700 3000 50  0001 C CNN
F 3 "" H 4700 3000 50  0001 C CNN
	1    4700 3000
	0    -1   -1   0   
$EndComp
$Comp
L ears_xml-eagle-import:A4L-LOC #FRAME5
U 1 1 FD6EE4A8
P 900 6700
AR Path="/FD6EE4A8" Ref="#FRAME5"  Part="1" 
AR Path="/5EF91E25/FD6EE4A8" Ref="#FRAME5"  Part="1" 
F 0 "#FRAME5" H 900 6700 50  0001 C CNN
F 1 "A4L-LOC" H 900 6700 50  0001 C CNN
F 2 "" H 900 6700 50  0001 C CNN
F 3 "" H 900 6700 50  0001 C CNN
	1    900  6700
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:POT_USVERTICAL_PS R7
U 1 1 3DB810B0
P 6400 1900
AR Path="/3DB810B0" Ref="R7"  Part="1" 
AR Path="/5EF91E25/3DB810B0" Ref="R7"  Part="1" 
F 0 "R7" V 6200 1800 59  0000 L BNN
F 1 "50kB" V 6300 1800 59  0000 L BNN
F 2 "ears_xml:ALPS_POT_VERTICAL_PS" H 6400 1900 50  0001 C CNN
F 3 "" H 6400 1900 50  0001 C CNN
	1    6400 1900
	0    -1   1    0   
$EndComp
$Comp
L ears_xml-eagle-import:R-US_R0402 R8
U 1 1 650187DA
P 5800 1900
AR Path="/650187DA" Ref="R8"  Part="1" 
AR Path="/5EF91E25/650187DA" Ref="R8"  Part="1" 
F 0 "R8" H 5650 1959 59  0000 L BNN
F 1 "510" H 5650 1770 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 5800 1900 50  0001 C CNN
F 3 "" H 5800 1900 50  0001 C CNN
	1    5800 1900
	-1   0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C7
U 1 1 6C94872B
P 6800 2300
AR Path="/6C94872B" Ref="C7"  Part="1" 
AR Path="/5EF91E25/6C94872B" Ref="C7"  Part="1" 
F 0 "C7" H 6840 2325 59  0000 L BNN
F 1 "22p" H 6840 2135 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 6800 2300 50  0001 C CNN
F 3 "" H 6800 2300 50  0001 C CNN
	1    6800 2300
	0    -1   -1   0   
$EndComp
$Comp
L ears_xml-eagle-import:R-US_R0402 R6
U 1 1 96E7E6D7
P 5900 2200
AR Path="/96E7E6D7" Ref="R6"  Part="1" 
AR Path="/5EF91E25/96E7E6D7" Ref="R6"  Part="1" 
F 0 "R6" H 5750 2259 59  0000 L BNN
F 1 "10k" H 5750 2070 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 5900 2200 50  0001 C CNN
F 3 "" H 5900 2200 50  0001 C CNN
	1    5900 2200
	-1   0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:R-US_R0402 R4
U 1 1 98DC0D12
P 5000 2700
AR Path="/98DC0D12" Ref="R4"  Part="1" 
AR Path="/5EF91E25/98DC0D12" Ref="R4"  Part="1" 
F 0 "R4" H 4850 2759 59  0000 L BNN
F 1 "10k" H 4850 2570 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 5000 2700 50  0001 C CNN
F 3 "" H 5000 2700 50  0001 C CNN
	1    5000 2700
	-1   0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C3
U 1 1 E6364C71
P 4100 2900
AR Path="/E6364C71" Ref="C3"  Part="1" 
AR Path="/5EF91E25/E6364C71" Ref="C3"  Part="1" 
F 0 "C3" H 4140 2925 59  0000 L BNN
F 1 "68p" H 4140 2735 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4100 2900 50  0001 C CNN
F 3 "" H 4100 2900 50  0001 C CNN
	1    4100 2900
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0603 C5
U 1 1 DBA0A74E
P 4400 2700
AR Path="/DBA0A74E" Ref="C5"  Part="1" 
AR Path="/5EF91E25/DBA0A74E" Ref="C5"  Part="1" 
F 0 "C5" H 4440 2725 59  0000 L BNN
F 1 "4.7u" H 4440 2535 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4400 2700 50  0001 C CNN
F 3 "" H 4400 2700 50  0001 C CNN
	1    4400 2700
	0    -1   -1   0   
$EndComp
$Comp
L ears_xml-eagle-import:OPA1641 IC1
U 2 1 06F9203B
P 6800 2600
AR Path="/06F9203B" Ref="IC1"  Part="2" 
AR Path="/5EF91E25/06F9203B" Ref="IC1"  Part="2" 
F 0 "IC1" H 6900 2725 59  0000 L BNN
F 1 "OPA1641" H 6900 2400 59  0000 L BNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6800 2600 50  0001 C CNN
F 3 "" H 6800 2600 50  0001 C CNN
	2    6800 2600
	1    0    0    1   
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0603 C16
U 1 1 C0B7EA3F
P 7500 2600
AR Path="/C0B7EA3F" Ref="C16"  Part="1" 
AR Path="/5EF91E25/C0B7EA3F" Ref="C16"  Part="1" 
F 0 "C16" H 7540 2625 59  0000 L BNN
F 1 "4.7u" H 7540 2435 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 7500 2600 50  0001 C CNN
F 3 "" H 7500 2600 50  0001 C CNN
	1    7500 2600
	0    -1   -1   0   
$EndComp
Text Notes 7300 5900 0    85   ~ 0
cc-by-sa
Text Notes 9400 5900 0    85   ~ 0
Input amplifier
Text Notes 6250 3100 0    59   ~ 0
Gain: 1x (0dB) to 100x (40dB)
Text Notes 3600 4000 0    59   ~ 0
LPF > 500kHz
Text Notes 4800 4000 0    59   ~ 0
V+ protection
Text Notes 6250 3300 0    59   ~ 0
LPF > 300kHz
Text Notes 3900 4200 0    59   ~ 0
AC coupling HPF < 0.02Hz
Text Notes 7300 6100 0    85   ~ 0
Émilie Gillet
Text Notes 7300 6300 0    85   ~ 0
emilie.o.gillet@gmail.com
Wire Wire Line
	5200 2700 6500 2700
Wire Wire Line
	7700 2600 7900 2600
Wire Wire Line
	7200 2600 7400 2600
Wire Wire Line
	3000 2800 3400 2800
Wire Wire Line
	4700 3200 4700 3400
Wire Wire Line
	4100 3100 4100 3400
Wire Wire Line
	3100 2900 3100 3100
Text GLabel 8300 3550 0    50   BiDi ~ 0
GND_PCB1
Text GLabel 8300 3050 0    50   BiDi ~ 0
GND_PCB1
Text GLabel 4800 3400 2    50   BiDi ~ 0
GND_PCB1
$Comp
L ears_xml-eagle-import:M02TINY MIC
U 1 1 9FD73AE4
P 3500 3400
AR Path="/9FD73AE4" Ref="MIC"  Part="1" 
AR Path="/5EF91E25/9FD73AE4" Ref="MIC1"  Part="1" 
F 0 "MIC1" H 3400 3630 59  0000 L BNN
F 1 "M02TINY" H 3400 3200 59  0000 L BNN
F 2 "ears_xml:1X02_TINY" H 3500 3400 50  0001 C CNN
F 3 "" H 3500 3400 50  0001 C CNN
	1    3500 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 3000 3800 3400
Wire Wire Line
	3800 3400 4100 3400
Connection ~ 4100 3400
Wire Wire Line
	4100 3400 4700 3400
Text GLabel 5500 2450 2    50   BiDi ~ 0
GND_PCB1
Text GLabel 3100 3100 0    50   BiDi ~ 0
GND_PCB1
Wire Wire Line
	5500 2200 5500 2450
Wire Wire Line
	5500 1900 5500 2200
Wire Wire Line
	8300 2800 8300 3050
Text GLabel 8300 4100 0    50   BiDi ~ 0
GND_PCB2
Wire Wire Line
	7200 1900 8200 1900
Wire Wire Line
	8300 3550 8300 4100
Wire Wire Line
	4800 3400 4700 3400
Connection ~ 4700 3400
NoConn ~ 8400 2700
$EndSCHEMATC
