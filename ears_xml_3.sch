EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 11774 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2550 1150 2550 1250
Wire Wire Line
	2550 1250 2550 1350
Wire Wire Line
	1850 1150 2450 1150
Wire Wire Line
	2450 1150 2550 1150
Wire Wire Line
	2550 1250 2450 1250
Wire Wire Line
	2450 1250 1850 1250
Wire Wire Line
	1850 1350 2450 1350
Wire Wire Line
	2450 1350 2550 1350
Wire Wire Line
	2550 1350 2650 1350
Wire Wire Line
	2650 1350 2650 1450
Connection ~ 2550 1250
Connection ~ 2550 1350
Connection ~ 2450 1350
Connection ~ 2450 1250
Connection ~ 2450 1150
Text GLabel 2550 1150 0    10   BiDi ~ 0
GND
Wire Wire Line
	3600 3500 4100 3500
Connection ~ 4100 3500
Wire Wire Line
	950  1450 950  1550
Wire Wire Line
	1050 1450 950  1450
Text GLabel 950  1450 0    10   BiDi ~ 0
VEE
Wire Wire Line
	950  950  950  1050
Wire Wire Line
	1050 1050 950  1050
Text GLabel 950  950  0    10   BiDi ~ 0
VCC
Wire Wire Line
	1850 1050 2450 1050
Wire Wire Line
	1750 1050 1850 1050
Connection ~ 1850 1050
Wire Wire Line
	1850 1450 2450 1450
Wire Wire Line
	1750 1450 1850 1450
Connection ~ 1850 1450
Wire Wire Line
	1550 1050 1450 1050
Wire Wire Line
	1450 1450 1550 1450
$Comp
L ears_xml-eagle-import:GND #GND1
U 1 1 DB8695AC
P 2650 1550
AR Path="/DB8695AC" Ref="#GND1"  Part="1" 
AR Path="/5EF923F4/DB8695AC" Ref="#GND01"  Part="1" 
F 0 "#GND01" H 2650 1550 50  0001 C CNN
F 1 "GND" H 2550 1450 59  0000 L BNN
F 2 "" H 2650 1550 50  0001 C CNN
F 3 "" H 2650 1550 50  0001 C CNN
	1    2650 1550
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:VEE #SUPPLY1
U 1 1 E46721A6
P 950 1650
AR Path="/E46721A6" Ref="#SUPPLY1"  Part="1" 
AR Path="/5EF923F4/E46721A6" Ref="#SUPPLY01"  Part="1" 
F 0 "#SUPPLY01" H 950 1650 50  0001 C CNN
F 1 "VEE" H 875 1775 59  0000 L BNN
F 2 "" H 950 1650 50  0001 C CNN
F 3 "" H 950 1650 50  0001 C CNN
	1    950  1650
	-1   0    0    1   
$EndComp
$Comp
L ears_xml-eagle-import:M05X2PTH JP4
U 1 1 47274FB3
P 2150 1250
AR Path="/47274FB3" Ref="JP4"  Part="1" 
AR Path="/5EF923F4/47274FB3" Ref="JP4"  Part="1" 
F 0 "JP4" H 2050 1580 59  0000 L BNN
F 1 "M05X2PTH" H 2050 850 59  0000 L BNN
F 2 "ears_xml:AVR_ICSP" H 2150 1250 50  0001 C CNN
F 3 "" H 2150 1250 50  0001 C CNN
	1    2150 1250
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:VCC #P+1
U 1 1 BCDF72C5
P 950 950
AR Path="/BCDF72C5" Ref="#P+1"  Part="1" 
AR Path="/5EF923F4/BCDF72C5" Ref="#P+01"  Part="1" 
F 0 "#P+01" H 950 950 50  0001 C CNN
F 1 "VCC" H 910 1090 59  0000 L BNN
F 2 "" H 950 950 50  0001 C CNN
F 3 "" H 950 950 50  0001 C CNN
	1    950  950 
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:GND #GND26
U 1 1 9E0685E6
P 3600 3800
AR Path="/9E0685E6" Ref="#GND26"  Part="1" 
AR Path="/5EF923F4/9E0685E6" Ref="#GND026"  Part="1" 
F 0 "#GND026" H 3600 3800 50  0001 C CNN
F 1 "GND" H 3500 3700 59  0000 L BNN
F 2 "" H 3600 3800 50  0001 C CNN
F 3 "" H 3600 3800 50  0001 C CNN
	1    3600 3800
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:DIODE-SOD523 D2
U 1 1 19794097
P 1650 1450
AR Path="/19794097" Ref="D2"  Part="1" 
AR Path="/5EF923F4/19794097" Ref="D2"  Part="1" 
F 0 "D2" H 1750 1469 59  0000 L BNN
F 1 "PMEG2010AEB" H 1350 1259 59  0000 L BNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1650 1450 50  0001 C CNN
F 3 "" H 1650 1450 50  0001 C CNN
	1    1650 1450
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:DIODE-SOD523 D1
U 1 1 B42AD0D0
P 1650 1050
AR Path="/B42AD0D0" Ref="D1"  Part="1" 
AR Path="/5EF923F4/B42AD0D0" Ref="D1"  Part="1" 
F 0 "D1" H 1750 1069 59  0000 L BNN
F 1 "PMEG2010AEB" H 1750 959 59  0001 L BNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1650 1050 50  0001 C CNN
F 3 "" H 1650 1050 50  0001 C CNN
	1    1650 1050
	-1   0    0    1   
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C9
U 1 1 27EB63CA
P 5000 2650
AR Path="/27EB63CA" Ref="C9"  Part="1" 
AR Path="/5EF923F4/27EB63CA" Ref="C9"  Part="1" 
F 0 "C9" H 5040 2675 59  0000 L BNN
F 1 "100n" H 5040 2485 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 5000 2650 50  0001 C CNN
F 3 "" H 5000 2650 50  0001 C CNN
	1    5000 2650
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C13
U 1 1 9AFB8C1C
P 4700 2650
AR Path="/9AFB8C1C" Ref="C13"  Part="1" 
AR Path="/5EF923F4/9AFB8C1C" Ref="C13"  Part="1" 
F 0 "C13" H 4740 2675 59  0000 L BNN
F 1 "100n" H 4740 2485 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4700 2650 50  0001 C CNN
F 3 "" H 4700 2650 50  0001 C CNN
	1    4700 2650
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C12
U 1 1 5225900B
P 4700 4500
AR Path="/5225900B" Ref="C12"  Part="1" 
AR Path="/5EF923F4/5225900B" Ref="C12"  Part="1" 
F 0 "C12" H 4740 4525 59  0000 L BNN
F 1 "100n" H 4740 4335 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4700 4500 50  0001 C CNN
F 3 "" H 4700 4500 50  0001 C CNN
	1    4700 4500
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC1206 C1
U 1 1 C20FC76C
P 4100 2650
AR Path="/C20FC76C" Ref="C1"  Part="1" 
AR Path="/5EF923F4/C20FC76C" Ref="C1"  Part="1" 
F 0 "C1" H 4140 2675 59  0000 L BNN
F 1 "22u" H 4140 2485 59  0000 L BNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4100 2650 50  0001 C CNN
F 3 "" H 4100 2650 50  0001 C CNN
	1    4100 2650
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC1206 C2
U 1 1 8A8AF94F
P 4100 4500
AR Path="/8A8AF94F" Ref="C2"  Part="1" 
AR Path="/5EF923F4/8A8AF94F" Ref="C2"  Part="1" 
F 0 "C2" H 4140 4525 59  0000 L BNN
F 1 "22u" H 4140 4335 59  0000 L BNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4100 4500 50  0001 C CNN
F 3 "" H 4100 4500 50  0001 C CNN
	1    4100 4500
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:A4L-LOC #FRAME8
U 1 1 DD6D1993
P 900 6500
AR Path="/DD6D1993" Ref="#FRAME8"  Part="1" 
AR Path="/5EF923F4/DD6D1993" Ref="#FRAME8"  Part="1" 
F 0 "#FRAME8" H 900 6500 50  0001 C CNN
F 1 "A4L-LOC" H 900 6500 50  0001 C CNN
F 2 "" H 900 6500 50  0001 C CNN
F 3 "" H 900 6500 50  0001 C CNN
	1    900  6500
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:WE-CBF_0603 L1
U 1 1 4CAB314C
P 1250 1150
AR Path="/4CAB314C" Ref="L1"  Part="1" 
AR Path="/5EF923F4/4CAB314C" Ref="L1"  Part="1" 
F 0 "L1" H 1100 1350 59  0000 L BNN
F 1 "WE-CBF_0603" H 1100 1100 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P7.62mm_Vertical" H 1250 1150 50  0001 C CNN
F 3 "" H 1250 1150 50  0001 C CNN
	1    1250 1150
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:WE-CBF_0603 L2
U 1 1 77B97637
P 1250 1550
AR Path="/77B97637" Ref="L2"  Part="1" 
AR Path="/5EF923F4/77B97637" Ref="L2"  Part="1" 
F 0 "L2" H 1100 1750 59  0000 L BNN
F 1 "WE-CBF_0603" H 1100 1500 59  0000 L BNN
F 2 "Resistor_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P7.62mm_Vertical" H 1250 1550 50  0001 C CNN
F 3 "" H 1250 1550 50  0001 C CNN
	1    1250 1550
	1    0    0    -1  
$EndComp
Wire Notes Line
	3700 2800 3800 2600
Wire Notes Line
	3800 2600 3900 2800
Wire Notes Line
	3900 2800 3700 2800
Text Notes 7300 5700 0    85   ~ 0
cc-by-sa
Text Notes 9500 5700 0    85   ~ 0
Power supply
Text Notes 3787 2750 0    59   ~ 0
~
Text Notes 3700 3450 0    34   ~ 0
6µF @ 11.5V
Text Notes 7300 5900 0    85   ~ 0
Émilie Gillet
Text Notes 7300 6100 0    85   ~ 0
emilie.o.gillet@gmail.com
$Comp
L ears_xml-eagle-import:C-USC0402 C4
U 1 1 284EA6E9
P 4400 2650
AR Path="/284EA6E9" Ref="C4"  Part="1" 
AR Path="/5EF923F4/284EA6E9" Ref="C4"  Part="1" 
F 0 "C4" H 4440 2675 59  0000 L BNN
F 1 "100n" H 4440 2485 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4400 2650 50  0001 C CNN
F 3 "" H 4400 2650 50  0001 C CNN
	1    4400 2650
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:C-USC0402 C6
U 1 1 FE64C8E2
P 4400 4500
AR Path="/FE64C8E2" Ref="C6"  Part="1" 
AR Path="/5EF923F4/FE64C8E2" Ref="C6"  Part="1" 
F 0 "C6" H 4440 4525 59  0000 L BNN
F 1 "100n" H 4440 4335 59  0000 L BNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 4400 4500 50  0001 C CNN
F 3 "" H 4400 4500 50  0001 C CNN
	1    4400 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 700  4100 800 
$Comp
L ears_xml-eagle-import:VCC #P+5
U 1 1 33ACA7EC
P 4100 700
AR Path="/33ACA7EC" Ref="#P+5"  Part="1" 
AR Path="/5EF923F4/33ACA7EC" Ref="#P+05"  Part="1" 
F 0 "#P+05" H 4100 700 50  0001 C CNN
F 1 "VCC" H 4060 840 59  0000 L BNN
F 2 "" H 4100 700 50  0001 C CNN
F 3 "" H 4100 700 50  0001 C CNN
	1    4100 700 
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:VEE #SUPPLY4
U 1 1 6DF68C43
P 4100 6700
AR Path="/6DF68C43" Ref="#SUPPLY4"  Part="1" 
AR Path="/5EF923F4/6DF68C43" Ref="#SUPPLY04"  Part="1" 
F 0 "#SUPPLY04" H 4100 6700 50  0001 C CNN
F 1 "VEE" H 4025 6825 59  0000 L BNN
F 2 "" H 4100 6700 50  0001 C CNN
F 3 "" H 4100 6700 50  0001 C CNN
	1    4100 6700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 2450 4400 2450
Wire Wire Line
	4400 2450 4400 2550
Wire Wire Line
	5600 2350 4700 2350
Wire Wire Line
	4700 2350 4700 2550
Wire Wire Line
	5800 2250 5000 2250
Wire Wire Line
	5000 2250 5000 2550
Wire Wire Line
	5400 4800 4400 4800
Wire Wire Line
	4400 4700 4400 4800
Wire Wire Line
	5600 4900 4700 4900
Wire Wire Line
	4700 4700 4700 4900
Connection ~ 4400 2450
Connection ~ 4100 800 
$Comp
L ears_xml-eagle-import:OPA1641 IC1
U 1 1 06F92037
P 5400 3500
AR Path="/06F92037" Ref="IC1"  Part="1" 
AR Path="/5EF923F4/06F92037" Ref="IC1"  Part="1" 
F 0 "IC1" H 5500 3625 59  0001 L BNN
F 1 "OPA1641" H 5500 3300 59  0001 L BNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5400 3500 50  0001 C CNN
F 3 "" H 5400 3500 50  0001 C CNN
	1    5400 3500
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:LM324PW IC2
U 5 1 541775DA
P 5800 3500
AR Path="/541775DA" Ref="IC2"  Part="5" 
AR Path="/5EF923F4/541775DA" Ref="IC2"  Part="5" 
F 0 "IC2" H 5900 3625 59  0001 L BNN
F 1 "LM324PW" H 5900 3300 59  0001 L BNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5800 3500 50  0001 C CNN
F 3 "" H 5800 3500 50  0001 C CNN
	5    5800 3500
	1    0    0    -1  
$EndComp
$Comp
L ears_xml-eagle-import:LM324PW IC3
U 5 1 5ED25CB7
P 5600 3500
AR Path="/5ED25CB7" Ref="IC3"  Part="5" 
AR Path="/5EF923F4/5ED25CB7" Ref="IC3"  Part="5" 
F 0 "IC3" H 5700 3625 59  0001 L BNN
F 1 "LM324PW" H 5700 3300 59  0001 L BNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5600 3500 50  0001 C CNN
F 3 "" H 5600 3500 50  0001 C CNN
	5    5600 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2850 4100 3500
Wire Wire Line
	5400 2450 5400 3200
Wire Wire Line
	5600 2350 5600 3200
Wire Wire Line
	5800 2250 5800 3200
Wire Wire Line
	5400 3800 5400 4800
Wire Wire Line
	5600 3800 5600 4900
Text GLabel 4350 3000 0    50   BiDi ~ 0
GND_PCB1
Text GLabel 4650 3200 0    50   BiDi ~ 0
GND_PCB2
Wire Wire Line
	4400 1850 4700 1850
Connection ~ 4700 2350
Wire Wire Line
	4100 800  5000 800 
Wire Wire Line
	4100 800  4100 2550
Wire Wire Line
	4700 1350 5000 1350
Wire Wire Line
	5000 1350 5000 2250
Connection ~ 5000 2250
Wire Wire Line
	4100 3500 4100 4400
Wire Wire Line
	4400 5450 4700 5450
Connection ~ 4400 4800
Wire Wire Line
	4700 4900 4700 5450
Connection ~ 4700 4900
Wire Wire Line
	4700 5950 5000 5950
Wire Wire Line
	4100 4700 4100 6500
Wire Wire Line
	5000 6500 4100 6500
Connection ~ 4100 6500
Wire Wire Line
	4100 6500 4100 6600
Text GLabel 5050 1350 2    50   BiDi ~ 0
VCC_PCB3
Text GLabel 5050 5950 2    50   BiDi ~ 0
VEE_PCB3
Text GLabel 4950 3400 0    50   BiDi ~ 0
GND_PCB3
Wire Wire Line
	4400 2850 4400 3000
Wire Wire Line
	4700 2850 4700 3200
Wire Wire Line
	5000 4100 5800 4100
Wire Wire Line
	5800 3800 5800 4100
Wire Wire Line
	5000 2850 5000 3400
Text GLabel 3550 3500 0    50   BiDi ~ 0
GND_PCB4
Wire Wire Line
	4700 1350 4700 1850
Connection ~ 4700 1850
Wire Wire Line
	4700 1850 4700 2350
Wire Wire Line
	4400 1850 4400 2450
Wire Wire Line
	5000 800  5000 1350
Connection ~ 5000 1350
Wire Wire Line
	4400 4800 4400 5450
Wire Wire Line
	4700 5450 4700 5950
Connection ~ 4700 5450
Wire Wire Line
	5000 5950 5000 6500
Text GLabel 4000 6500 0    50   BiDi ~ 0
VEE
Wire Wire Line
	3550 3500 3600 3500
Connection ~ 3600 3500
Wire Wire Line
	4400 3000 4350 3000
Connection ~ 4400 3000
Wire Wire Line
	4400 3000 4400 4400
Wire Wire Line
	4700 3200 4650 3200
Connection ~ 4700 3200
Wire Wire Line
	4700 3200 4700 4400
Wire Wire Line
	5000 3400 4950 3400
Connection ~ 5000 3400
Wire Wire Line
	5000 3400 5000 4100
Wire Wire Line
	5000 1350 5050 1350
Wire Wire Line
	5000 5950 5050 5950
Connection ~ 5000 5950
Wire Wire Line
	4100 6500 4000 6500
Wire Wire Line
	3600 3500 3600 3650
Text GLabel 3550 3650 0    50   BiDi ~ 0
GND
Wire Wire Line
	3550 3650 3600 3650
Connection ~ 3600 3650
Wire Wire Line
	3600 3650 3600 3700
Text GLabel 4000 800  0    50   BiDi ~ 0
VCC
Wire Wire Line
	4000 800  4100 800 
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5FE6552F
P 950 1050
F 0 "#FLG0101" H 950 1125 50  0001 C CNN
F 1 "PWR_FLAG" H 950 1223 50  0000 C CNN
F 2 "" H 950 1050 50  0001 C CNN
F 3 "~" H 950 1050 50  0001 C CNN
	1    950  1050
	-1   0    0    1   
$EndComp
Connection ~ 950  1050
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5FE675CD
P 950 1450
F 0 "#FLG0102" H 950 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 950 1623 50  0000 C CNN
F 2 "" H 950 1450 50  0001 C CNN
F 3 "~" H 950 1450 50  0001 C CNN
	1    950  1450
	1    0    0    -1  
$EndComp
Connection ~ 950  1450
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5FE6A558
P 2550 1150
F 0 "#FLG0103" H 2550 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 1323 50  0000 C CNN
F 2 "" H 2550 1150 50  0001 C CNN
F 3 "~" H 2550 1150 50  0001 C CNN
	1    2550 1150
	1    0    0    -1  
$EndComp
Connection ~ 2550 1150
Text Notes 1000 6900 0    50   ~ 0
Paper silkscreen mounts
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5EFE5FD4
P 1350 7050
F 0 "J5" H 1430 7092 50  0000 L CNN
F 1 "Conn_01x01" H 1430 7001 50  0000 L CNN
F 2 "Connector_Pin:Pin_D0.7mm_L6.5mm_W1.8mm_FlatFork" H 1350 7050 50  0001 C CNN
F 3 "~" H 1350 7050 50  0001 C CNN
	1    1350 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5EFE6C04
P 1350 7200
F 0 "J6" H 1430 7242 50  0000 L CNN
F 1 "Conn_01x01" H 1430 7151 50  0000 L CNN
F 2 "Connector_Pin:Pin_D0.7mm_L6.5mm_W1.8mm_FlatFork" H 1350 7200 50  0001 C CNN
F 3 "~" H 1350 7200 50  0001 C CNN
	1    1350 7200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 5EFE8382
P 1350 7350
F 0 "J7" H 1430 7392 50  0000 L CNN
F 1 "Conn_01x01" H 1430 7301 50  0000 L CNN
F 2 "Connector_Pin:Pin_D0.7mm_L6.5mm_W1.8mm_FlatFork" H 1350 7350 50  0001 C CNN
F 3 "~" H 1350 7350 50  0001 C CNN
	1    1350 7350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5EFE9ACD
P 1350 7500
F 0 "J8" H 1430 7542 50  0000 L CNN
F 1 "Conn_01x01" H 1430 7451 50  0000 L CNN
F 2 "Connector_Pin:Pin_D0.7mm_L6.5mm_W1.8mm_FlatFork" H 1350 7500 50  0001 C CNN
F 3 "~" H 1350 7500 50  0001 C CNN
	1    1350 7500
	1    0    0    -1  
$EndComp
NoConn ~ 1150 7050
NoConn ~ 1150 7200
NoConn ~ 1150 7350
NoConn ~ 1150 7500
$EndSCHEMATC
